import React, { useState } from "react";

export default function AddUser() {
  const [inputs, setInputs] = useState({});

  const add_user = (e) => {
    e.preventDefault();
    const login = document.forms[0]["login"].value;
    const avatar = document.forms[0]["avatar_url"].value;
    const github = document.forms[0]["github_url"].value;
    let arr=[];
    let obj ={"login": login,
              "avatar_url":avatar,
              "github_url":github};
    arr.push(obj);
    console.log(arr);
    // return arr ;
  };

  return (
    <div>
      <div className="container">
        <form onSubmit={add_user}>
          <div className="mb-3">
            <label htmlFor="login" className="form-label">
              Login
            </label>
            <input
              type="text"
              className="form-control"
              id="login"
              name="login"
            />
          </div>
          <div className="mb-3">
            <label htmlFor="avatar" className="form-label">
              avatar
            </label>
            <input
              type="text"
              className="form-control"
              id="avatar"
              name="avatar_url"
              required
            />
          </div>
          <div className="mb-3">
            <label htmlFor="avatar" className="form-label">
              Github url
            </label>
            <input
              type="text"
              className="form-control"
              id="github"
              name="github_url"
              required
            />
          </div>
          <div className="mb-3">
            <input type="submit" className="btn btn-primary" value="submit" />
          </div>
        </form>
      </div>
    </div>
  );
}

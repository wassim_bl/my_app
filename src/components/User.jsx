import React, { useEffect, useState } from "react";

export default function Users() {
  const url = "http://localhost:8002/users";
  const [users, setUsers] = useState([]);

  const fetchData = () => {
    fetch(url)
      .then((response) => response.json())
      .then((data) => {
        setUsers(data);
        // console.log(data);
      });
  };

  useEffect(() => {
    fetchData();
  });

  return (
    <>
      <div className="container">
        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-4 g-4">
          {users.map((user) => (
            <div className="col">
              <div className="card">
                <img src={user.avatar_url} className="card-img-top" alt="..." />
                <div className="card-body">
                  <h5 className="card-title">{user.login}</h5>
                  <p className="card-text">
                    Some quick example text to build on the card title and make
                    up the bulk of the card's content.
                  </p>
                  <a href={user.html_url} className="btn btn-primary">hithub Profile</a>
                </div>
              </div>
            </div>
          ))}
        </div>
      </div>
    </>
  );
}
